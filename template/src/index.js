import * as nodeanimate from "nodeanimate";
import Input from "nodeanimate/src/Input/input";

const width = 720;
const height = 405;

//const container = document.getElementById('n');
const container = n; // auto-eval ids
const loop = nodeanimate.create2d({container, height, width});

loop.add({
  count: 0,
  p: {x: 1, y: 1},
  update: function(context){
    const {p} = this;
    if (Input.keyboard.isKeyDown("ArrowUp")) {
      p.y -= 1;
    }
    if (Input.keyboard.isKeyDown("ArrowDown")) {
      p.y += 1;
    }
    if (Input.keyboard.isKeyDown("ArrowLeft")) {
      p.x -= 1;
    }
    if (Input.keyboard.isKeyDown("ArrowRight")) {
      p.x += 1;
    }
    if (p.x < 0) { p.x = width; }
    if (p.y < 0) { p.y = height; }
    if (p.x > width) { p.x = 0; }
    if (p.y > height) { p.y = 0; }
  },
  draw: function(context){
    context.save();
    context.setTransform(1,0,0,1,0,0);
    context.fillStyle = "cornflowerblue";
    context.fillRect(0, 0, width, height);
    context.translate(this.p.x, this.p.y);
    context.fillStyle = "red";
    context.fillRect(-5, -5, 10, 10);
    context.restore();
  }
});

