const path = require("path");
module.exports = {
  mode: process.env.WEBPACK_SERVE ? "development" : "production",
  serve: {
    content: path.resolve( __dirname, "public"),
  },
}
