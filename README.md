NodeAnimate
===========
A simple update-draw loop for html canvas 2d.  Handles input from mouse, keyboard, and (coming soon) touch and gamepad.  Other features are being gradually ported from prior art, [gameloop](https://bitbucket.org/sbattin/gameloop).

New Project Quickstart
----------
To create a new project, run this one line of code (assumes preexisting node/npm):
```bash
npx nodeanimate myprojectname
```

`npx` will execute the default "bin" of this library, which is [scripts/init.js](https://bitbucket.org/sbattin/nodeanimate/src/master/scripts/init.js).  It does the following:

1. `mkdir myprojectname` (and do everything else inside of it)
1. initialize an npm project
1. initialize a git repo
1. run `scripts/seed.js` (adds all the project files from /template)
1. run `npm install` everything (mostly webpack)
1. do an initial git commit of all files using a sensible .gitignore 

Then to see the animation:
```
cd myprojectname && npm start
```
This will run a webpack-based server and open a browser to view it.  If everything worked, you should see a cornflowerblue canvas.  Arrow keys move the red box.  The server includes hot-reloading, so editing `/src/index.js` will update in the browser without requiring a page reload. 

The project also includes an npm script to create the minified code.
```bash
npm run build
```
This script creates the minified js and adds it to a zip file along with the minimal html.  The file is `dist/js13k.zip`, because this library was intended for creating entries to that contest.

New Stuff
---------

### Uses webpack

...instead of endless scripts in tags in html.  Vanilla javascript suffers from order-dependence, script inclusion, and no such thing as scope control other than closures and globals.  Webpack makes javascript be logical in the face of separated modules, without manual handling of dependencies.  And best of all; _it's not that hard_.  The memes about configuring webpack are lies.  It was trivial to specify a main js file, start typing `import` all over the place, and generate a single script to include in my html doc.  I strongly suspect webpack's rep for confusion stems from starting with an ungainly codebase, trying to get it under control and failing, and then blaming webpack.  I say it wasn't webpack's fault for those failures.

### JS13k-ready
I wanted to make boilerplate that would work for the [js13k game jam](http://js13kgames.com).  tl;dr, Make a game that runs in Chrome and Firefox.  So:
```
npm run build
```
...to generate a contest submission in `/dist`

Contest rules are that you can write whatever source code you want, as long as it zips down to under 13kb with no dynamic loading.  It's not one of those grueling 72-hour marathon game jams where participation is impossible for anyone with a sleep schedule; you get a month.  Neverless, I wanted to be able to immediately start on the game when the time came, having engine already in hand.  My previous work was unsuitable.  And webpack makes it trivial (again) to minify and zip the output.

### not-babel

Yeah, ES6 is great and all, but it's also kindof terrible.  And I'm not using a single facebook library, so no need to "trans-pile" anything.  I just want javascript that understands modules.  The contest rules only require latest Chrome and Firefox support, so lots of ES6 is already available there.  But feel free to add whatever extra babel shenanigans you want; it's just not built-in.

Old Stuff
---------

I imported near-verbatim my old libraries for 2D Camera and Input consolidating.  I also stuck with my previous pattern for being able to utilize any js object that has `update` or `draw` or both.  I cleaned up my initialization code a fair bit, and I trimmed a lot of garbage.

I _might_ try to ressurect my WebRTC experiment for peer-to-peer networking [DataFestivus](https://github.com/sethbattin/datafestivus).  But it's got low priority because it would bloat this ostensibly-small js game engine, and if i want to make a multiplayer game, i could just use the socket server that they are providing.

I definitely will reimplement the sound and drawing library features.  Must-haves.

Immediate TODO
--------------
1. touch input
1. generate animation frame bitmaps from svg, css, transforms, and data urls
1. add back example to the repo so that it can run from a fork

### Also TODO
~~Configure webpack hotloading (replace express?)~~ v4
~~make it easy to remove stuff (debug, etc.) from the seeded app.js~~ v5
use Minimist to make scripts have options
~~separate input from example app for clarity in app.js~~
make fullscreen meta tags and css, but skippable for js13k
port the Clear component, add fading in and out
make a github.io page for the example
change camera.setTransform to .applyTransform (disambiguate from a setter)
