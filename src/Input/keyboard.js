function keyboard(receiver) {
  if (!(this instanceof keyboard)){
    return new keyboard(receiver);
  }
  var PROP = "key";
  var transform = function(prop) { return ("" + prop).toLowerCase(); };
  // support browsers without 'key' event property?  but that do support canvas?
  /* receiver.addEventListener('keydown', function keyEventAnalyze (e){
        var backups = ["which", "keyCode", "char"];
        while(!e.hasOwnProperty(PROP) && backups.length){
            PROP = backups.shift();
            transform = function(prop) {return prop;};
        }
        receiver.removeEventListener('keydown', keyEventAnalyze);
    });
  */
  this.keys = {};
  var self = this;
  receiver.addEventListener("keydown", function(e) {
    console.log("keydown for e[" + PROP +"] " + e[PROP] + " (as '" + transform(e[PROP]) + "')", e.keyCode, e);
    if (!self.keys[transform(e[PROP])]){
      self.keys[transform(e[PROP])] = Date.now();
    }
  });
  receiver.addEventListener("keyup", function(e) {
    delete self.keys[transform(e[PROP])];
  });
  this.isKeyDown = function(){ // ...arguments
    return arguments.length > 0 && (
      !!this.keys[transform(arguments[0])] || 
      this.isKeyDown.apply(this, Array.prototype.slice.apply(arguments, [1]))
    );
  };
  this.isKeyHeld = function(k, duration){
    return (this.keys[k] + duration) < Date.now(); // undefined + Number === NaN, NaN < number === false
  };
  this.anyKey = function(){
    return Object.keys(this.keys).length > 0;
  };
}

export default keyboard;

