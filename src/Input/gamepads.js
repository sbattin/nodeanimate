export default function gamepads(canvas){
  var gps = {};
  var navGps = [];
  var navTime = 0;
  function updateNav(time){
    if (time !== navTime) {
      navTime = time;
      
      navGps = canvas.ownerDocument.defaultView.navigator.getGamepads();
    }
  }

  function padUpdate(context, delta, time){
    updateNav(time);
    if (!navGps[this.id]){
      this.disconnect();
    } else {
      this.buttons_prev = this.buttons;
      this.buttons = navGps[this.id].buttons;
      this.axes_prev = this.axes;
      this.axes = navGps[this.id].axes;
    }
  }
  
  function pad(id){
    this.id = id;
    this.buttons_prev = {};
    this.buttons = {};
    this.axes_prev = {};
    this.axes = {}
    this.update = padUpdate.bind(this);
    this.disconnect = function(){};

    this.update(void(0), void(0), navTime);
  }

  canvas.ownerDocument.addEventListener("gamepadconnected", function(e){
    gps[e.gamepad.index] = new pad(e.gamepad.index);
    _connected(e.gamepad.index);
  });
  canvas.ownerDocument.addEventListener("gamepaddisconnected", function(e){
    gps[e.gamepad.index].disconnect();
    delete gps[e.gamepad.index];
  });

  this.getGamepadPush = function(time){
    var result;
    updateNav(time);

    Array.prototype.forEach.call(navGps, function(gp) {
      if (!gp) { return; }
      if (result) { return; }
      gp.buttons.forEach(function(button) {
        if (! result) {
          if (button.pressed){
            result = new pad(gp.index);
          }
        }
      });
    });
    return result;
  }
  this.getGamepadFirst = function(time){
    var result;
    updateNav(time);
    Array.prototype.forEach.call(navGps, function(gp) {
      if (!gp) { result = new pad(gp.index); }
    });
    return result;
  }
}

