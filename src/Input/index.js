import keyboard from "./keyboard";
import mouse from "./mouse";
import input from "./input";
import gamepads from "./gamepads"; 

export default input;

export {
  input,
  keyboard,
  mouse,
  gamepads,
};

