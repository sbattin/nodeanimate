function mouse(canvas){
  if (!(this instanceof mouse)){
    return new mouse(canvas);
  }
  var viewportPixelRatioX = canvas.width / canvas.clientWidth;
  var viewportPixelRatioY = canvas.height / canvas.clientHeight;
  var bcr = canvas.getBoundingClientRect();
  var getOffset = function(e){
    return {
      x: viewportPixelRatioX * (e.clientX - bcr.left),
      y: viewportPixelRatioY * (e.clientY - bcr.top),
    };
  }
  var self = this;
  this.tracking = false;
  this.position = {x: -1, y: -1};
  canvas.addEventListener("mouseover", function(){
    self.tracking = true;
  });
  canvas.addEventListener("mouseout", function(){
    self.tracking = false;
    self.position = {x: -1, y: -1};
  });
  canvas.addEventListener("mousemove", function(e){
    if (self.tracking){
      self.position = getOffset(e);
    }
  });

  var buttons = {};
  canvas.ownerDocument.addEventListener("mouseup", function(e){
    delete buttons[e.which];
  });
  canvas.addEventListener("mousedown", function(e){
    console.log("mouse event for " + e.which, e);
    buttons[e.which] = Date.now();
  });

  this.isPushed = function(which){
    return !!buttons[which];
  };
  this.anyButton = function(){
    return Object.keys(buttons).length > 0;
  };
  this.getPosition = function(){
    return position;
  };

};

export default mouse;
