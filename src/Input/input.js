import mouse from "./mouse";
import keyboard from "./keyboard";
import gamepads from "./gamepads";

var input = {
  mouse: void(0),
  keyboard: void(0),
  gamepads: void(0),
};

export function init(canvas){
  input.mouse = mouse(canvas);
  input.keyboard = keyboard(canvas.ownerDocument);
  input.gamepads = new gamepads(canvas);
}

export default input;
