import Input from "./Input";

var Debug = function(camera) {
  this.camera = camera;
  this.cursorx = 0;
  this.cursory = 0;
  this.update = function(context, timediff, timestamp) {
    if (Input.mouse.isPushed(1)){
      var p = Input.mouse.position;
      var pp = camera.getWorldCoords(p.x, p.y, context);
      this.cursorx = pp.x;
      this.cursory = pp.y;
    }

    if (Input.keyboard.isKeyDown("1")) {
      this.cursorx -= 1;
    }
    if (Input.keyboard.isKeyDown("2")) {
      this.cursorx += 1;
    }
    if (Input.keyboard.isKeyDown("3")) {
      this.cursory -= 1;
    }
    if (Input.keyboard.isKeyDown("4")) {
      this.cursory += 1;
    }
    if (Input.keyboard.isKeyDown("ArrowUp", 5)) {
      this.cursory -= 10;
    }
    if (Input.keyboard.isKeyDown("ArrowDown")) {
      this.cursory += 10;
    }
    if (Input.keyboard.isKeyDown("ArrowLeft")) {
      this.cursorx -= 10;
    }
    if (Input.keyboard.isKeyDown("ArrowRight")) {
      this.cursorx += 10;
    }
  };


  this.draw = function(context, timediff, timestamp) {
    this.camera.setTransform(context);
    
    drawCursor(context, 0, 0, 100, 50, "yellow", "purple");
    drawCursor(context, this.cursorx, this.cursory, 100, 0, "brown", "darkgreen");

    context.save();
    context.setTransform(1, 0, 0, 1, 0, 0);
   
    context.font = "bold 16px arial";

    var pos_text = 
      " x: " + Input.mouse.position.x.toFixed(1) + "," +
      " y: " + Input.mouse.position.y.toFixed(1) + " ";
    var text_width = context.measureText(pos_text).width;
    if (text_width > (context.canvas.width - 20 - Input.mouse.position.x)){
      context.textAlign = 'right';
    } else {
      context.textAlign = 'left';
    }
    if (Input.mouse.position.y < 20){
      context.textBaseline = 'top';
    }
    drawCursor(context, Input.mouse.position.x, Input.mouse.position.y, 50, 50);
    context.restore();

  };

  var drawCursor = function(context, x, y, size, intersect, color1, color2, alpha) {
    if (typeof intersect == "undefined"){
      intersect = 0;
    }
    if (typeof color1 == "undefined"){
      color1 = "white";
    }
    if (typeof color2 == "undefined"){
      color2 = color1;
    }
    if (typeof alpha == "undefined"){
      alpha = 0.5;
    }
    // ^- sure would be nice to have some ES6 default arguments right there, eh?
    var prevAlpha = context.globalAlpha;
    context.globalAlpha = alpha;

    var text = " x: " + x.toFixed(1) + ", y: " + y.toFixed(1) + " ";

    context.fillStyle = color1;
    context.fillText(text, x, y);
    context.fillRect(x - intersect, y, size, 1);
    context.fillStyle = color2;
    context.fillRect(x, y - intersect, 1, size);

    context.globalAlpha = prevAlpha;
  }

};


export default Debug;
