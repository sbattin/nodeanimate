const loadees = {};
const pixel = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
const ph = new Image();
ph.src = pixel;
export function loader (doc) {
  doc.images.forEach(function(e){
    loadees[e.id] = ph;
    if (el.complete){
      loadees[id] = el;
    } else if (el.src) {
      el.addEventListener("load", function() { loadees[id] = this; });
    }
  });
}

export default function Sprite(id){
  if (!(this instanceof Sprite)){ return new Sprite(id); }
  this.id = id;
  this.loaded = function() { return loadees[id] !== ph; };
  this.offset = [0.5, 0.5];
  this.rotation = 0;

  this.draw = function(context){
    context.save();
    context.rotate(this.rotation);
    context.drawImage(
      loadees[id],
      -1 * this.offset[0] * loadees[id].width,
      -1 * this.offset[1] * loadees[id].height,
      loadees[id].width,
      loadees[id].height
    );
  }
}
