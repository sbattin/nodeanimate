var Camera = function(){
  this.position = {x: 0, y: 0};
  this.zoom = 1;
  this.offset = {x: 0, y: 0};

  this.controller = null

  this.update = function(context) {

    if (this.controller){
      this.controller.control(this);
    }

    this.offset = {
      x: context.canvas.width / 2  - (this.position.x * this.zoom),
      y: context.canvas.height / 2  - (this.position.y * this.zoom)
    };
  };

  this.setTransform = function(context) {
    /*
    // ^- extra slash here
    console.log("setting transform\n["+ this.zoom.toFixed(2) + ", 0.00, " + this.offset.x.toFixed(2) + "]\n" + 
	"[0.00, " + this.zoom.toFixed(2) + ", " + this.offset.y.toFixed(2) + "]\n");
    /*/
      // console.log(context.currentTransform);
    //*/
    context.setTransform(this.zoom, 0, 0, this.zoom, this.offset.x, this.offset.y);
  };
  
  this.getWorldCoords = function(viewX, viewY, context) {
    var x = (context.canvas.width / context.canvas.offsetWidth) * viewX / this.zoom;
    var y = (context.canvas.height / context.canvas.offsetHeight) * viewY / this.zoom;
    return {
      x: (x - this.offset.x / this.zoom),
      y: (y - this.offset.y / this.zoom)
    };
  };

};

var LerpController = function(){
  this.position = {x: 0, y: 0};
  this.zoom = 1;
  this.lerpRate = 0.07;

  this.controller = null;

  this.control = function(camera){

    if (this.controller){
      this.controller.control(this);
    }

    camera.zoom += (this.zoom - camera.zoom) * this.lerpRate;
    camera.position.x += (this.position.x - camera.position.x) * this.lerpRate;
    camera.position.y += (this.position.y - camera.position.y) * this.lerpRate;

    if (Math.abs(this.zoom - camera.zoom) < 0.001){
      camera.zoom = this.zoom;
    }
    if (Math.abs(this.position.x - camera.position.x) < 0.001){
      camera.position.x = this.position.x;
    }
    if (Math.abs(this.position.y - camera.position.y) < 0.001){
      camera.position.y = this.position.y;
    }
  };

}

export default Camera;
export { LerpController };
