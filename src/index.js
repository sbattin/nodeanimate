import Loop from "./Loop";
import * as Camera  from "./Camera";
import Debug from "./Debug";
import { init } from "./Input/input";
import Input from "./Input";

function create2d(options){
  var canvas = document.createElement('canvas');
  canvas.height = options.height;
  canvas.width = options.width;
  options.container.appendChild(canvas);

  var context = canvas.getContext('2d');
  
  init(canvas);
  return new Loop(context);  
};

export {
  create2d,
  Loop,
  Camera,
  Debug,
  Input,
};

