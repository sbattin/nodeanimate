var targetRate = 33;
var animationFrame =  window.requestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.msRequestAnimationFrame ||
  function(f){ return window.setTimeOut(f, targetRate);};  // IE9?

var Loop = function(context){

  if (!(this instanceof Loop)){
    return new Loop(context);
  }
  
  var prevUpdate = 0;
  var prevDraw = 0;
  var updateables = [];
  var drawables = [];

  this.add = function(item){
    if (item && (typeof item.update === "function")){
      updateables.push(item);
    }
    if (item && (typeof item.draw === "function")){
      drawables.push(item);
    }
  };

  var deltaTime = function(time, prev) {
    var delta = time - prev;

    // TODO: implement window-visibility loss detection
    // http://stackoverflow.com/a/1060034/1004027
    // this is a stopgap
    delta = delta % (2 * targetRate);
   
    return delta;
  }

  var update = function(time){
    var delta = deltaTime(time, prevUpdate);
    var prevUpdate = time;
    for (var i = 0; i < updateables.length; i++){
      updateables[i].update(context, delta, time);
    }
  }
  var draw = function(time){
    var delta = deltaTime(time, prevDraw);
    var prevDraw = time;
    for (var i = 0; i < drawables.length; i++){
      drawables[i].draw(context, delta, time);
    }
  }

  var initialized = false;
  var loop = function(time) {
    animationFrame(loop);
    if (initialized){
      update(time);
      draw(time);
    }
  };
  animationFrame(loop);

  initialized = true;

};

module.exports = Loop;
