#!/usr/bin/env node

const { execSync } = require("child_process");
const { createInterface } = require("readline");
const { join } = require("path");
const { statSync } = require("fs");
const { log, error } = console;

const quit = (msg, code = 0) => {
  code ? error(msg) : log(msg);
  process.exit(code);
};

const logExec = (cmd) => {
  log(cmd);
  try {
    execSync(cmd);
  } catch (e) {
    quit(`command '${cmd}' failed; quitting.`, 1);
  }
};

const isFile = (path) => {
  try {
    return statSync(path).isFile();
  } catch (e) {
    if (e.code !== "ENOENT") {
      throw e;
    }
  }
  return false;
}

const seed = (name) => {
  logExec(`mkdir ${name}`);
  logExec(`cd ${name} && git init `);
  logExec(`cd ${name} && npm init -y`);
  logExec(`cd ${name} && npm install nodeanimate --save`);
  logExec(`cd ${name} && node ${__dirname}/seed.js`);
  logExec(`cd ${name} && npm install`);
  logExec(`cd ${name} && git add . && git commit -m'nodeanimate project init'`);

  quit(`Project "${name}" created.`);
}

// check for being inside a node project and reject
const pathPDJ = execSync("npm prefix", {encoding: "utf-8"}).trim();
if (
  pathPDJ !== process.cwd() // in a project sub directory
  || isFile(`${pathPDJ}/package.json`) // in project root
){
  quit("nodeanimate-init can not be run inside an existing npm project.", 2)
}

// read first argument or prompt for name for new directory
const projectName = process.argv[2];
if (projectName) {
  seed(projectName);
} else {
  const rl = createInterface({input: process.stdin, output: process.stdout})
  rl.question("Enter project name:", (answer) => {
    if (!answer){
      quit("empty project name not allowed; quitting.", 3);
    }
    seed(answer);
    rl.close();
  });
}
// move all the non-project template files into place
// npm install yada yada
// add the project template files
// prompt to run

