#!/usr/bin/env node

const { dirname, join, normalize } = require("path");
const { 
  existsSync,
  mkdirSync,
  readFileSync,
  statSync,
  writeFileSync,
  createReadStream,
  createWriteStream,
  appendFileSync,
  renameSync,
} = require("fs");
const { execSync } = require("child_process");
const { createInterface } = require("readline");

var PACKAGE_DOT_JSON = "package.json";
var PWD = process.env.PWD;
var NA_DIR = normalize(join( __dirname, ".." ));

const dirs = [
  "src",
  "scripts",
  "public",
];

const copies = exports.copies = [
  join("src", "index.js"),
  join("src", "appInput.js"),
  "webpack.config.js",
  join("scripts", "submission.js"),
  join("scripts", "check13k.js"),
  join("public", "index.html"),
];

const dependencies = exports.dependencies = {
  "webpack": "^4.0.0",
  "webpack-command": "^0.3.1",
  "webpack-serve": "^1.0.0",
  "yazl": "^2.4.3"
};

const scripts = exports.scripts = {
  "build": "webpack --run-prod",
  "postbuild": "node scripts/submission.js && node scripts/check13k.js",
  "start": "webpack-serve --open",
};

function createDir(name){
  const targetDir = join( PWD, name );
  try {
    statSync(targetDir);
  } catch (e) {
    if (e.code !== "ENOENT"){
      throw e;
    }
    mkdirSync(targetDir);
  }
}
exports.createDir = createDir;
function copyFile(filename, path) {
  const src = join(NA_DIR, path, filename);
  const dest = join(PWD, filename);
  writeFileSync(dest, readFileSync(src));
}
function addDependency(dep, version) {
  var addLine = `    "${dep}": "${version}",\n`;
  addToPDJ("dependencies", addLine);
}

function addScript(name, script) {
  var addLine = `    "${name}": "${script}",\n`;
  addToPDJ("scripts", addLine);
}
exports.addScripts = addScript; 

var promise;
function addToPDJ(section, addLine) {
  var p = () => { return new Promise((res, rej) => {
    var pdjBuffer = createReadStream(join(PWD, PACKAGE_DOT_JSON));
    var resultName = join(PWD, PACKAGE_DOT_JSON) + ".new";
    var writer = createWriteStream(resultName);
    var reader = createInterface({input: pdjBuffer});
    var lineTest = new RegExp(`(^\\s)*(${addLine.trim().split(":")[0]}:)`);
    reader.on("line", (line) => {
      if (!line.match(lineTest)){
        writer.write(line + "\n");
      }
      if (line.match(`"${section}"`)){
        writer.write(addLine);
      }
    });
    pdjBuffer.on("end", () => {
      writer.end();
      renameSync(resultName, join(PWD, PACKAGE_DOT_JSON));
      res();
    });
  });};

  promise = promise ? promise.then( p ) : p();
}
exports.addToPDJ = addToPDJ;
var main = function(){

usage = function(exitcode) {
  console.info("\nUsage:\n/project/with/dep/nodeanimate$ node_modules/nodeanimate/scripts/seed.js");
  process.exit(exitcode);
};

if (PWD === NA_DIR){
  console.error("This script should not be called within nodeanimate.");
  usage(1);
}

if (!existsSync(join(PWD, PACKAGE_DOT_JSON))){
  console.error(`\nDid not find file ${join(PWD, PACKAGE_DOT_JSON)}.  Run this script from an npm project root.`);
  usage(2);
}

dirs.forEach(createDir);
copies.forEach((name) => copyFile(name, "template"));
Object.keys(dependencies).forEach((dep) => addDependency(dep, dependencies[dep]));
Object.keys(scripts).forEach((script) => addScript(script, scripts[script]));

// because keeping a .gitignore file in the package appeared to be impossible (3.3.1 - 3.3.4)
writeFileSync(join(PWD, ".gitignore"), readFileSync(join(NA_DIR, "template", "git.ignore")));

console.log("\n\n**************** SUCCESS ****************\n\n");
console.log("This project has been seeded with files, dependencies, and scripts to build a browser animation.",
"  To view the default, run `npm i && npm start`, then open http://localhost:8080//\n\n\n");

}

if (require.main === module){
  main();
}
