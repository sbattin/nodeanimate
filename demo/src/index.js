import * as nodeanimate from "nodeanimate";
import { boxInput, camMoveController, gamepadThing } from "./appInput";

const width = 720;
const height = 405;

//const container = document.getElementById('container');
const container = container; // auto-eval ids
const loop = nodeanimate.create2d({container, height, width});

loop.add({
  count: 0,
  p: {x: 1, y: 1},
  update: function(context){
   const {p} = this;
    p.x += boxInput.x();
    p.y += boxInput.y();

    if (p.x < width / -2) { p.x = width / 2; }
    if (p.y < height / -2) { p.y = height / 2; }
    if (p.x > width / 2) { p.x = width / -2; }
    if (p.y > height / 2) { p.y = height / -2; }
  },
  draw: function(context){
    context.save();
    context.setTransform(1,0,0,1,0,0);
    context.fillStyle = "cornflowerblue";
    context.fillRect(0, 0, width, height);
    cam.setTransform(context);
    context.translate(this.p.x, this.p.y);
    context.fillStyle = "red";
    context.fillRect(-5, -5, 10, 10);
    context.restore();
  }
});

const cam = window.cam = new nodeanimate.Camera.default();
cam.controller = new nodeanimate.Camera.LerpController();
cam.controller.controller = camMoveController;
loop.add(cam);
loop.add(camMoveController);

window.gp = gamepadThing;

loop.add(gamepadThing);

loop.add(new nodeanimate.Debug(cam));

